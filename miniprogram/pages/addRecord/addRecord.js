// miniprogram/pages/addRecord/addRecord.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    todayDate:'',
    money:'',
    remarks:'',
    endDate: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let today = new Date()
    this.setData({
      todayDate: options.todayDate,
      endDate: today.getFullYear() + '-' + parseInt(today.getMonth() + 1)+'-'+today.getDate()
    })
    console.log(this.data.endDate)
  },

  bindDateChange(e) {
    // console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      todayDate: e.detail.value
    })
  },

  inputMoney:function(e){
    console.log(e)
    this.setData({
      money: e.detail.value.replace(/\s+/g, '')
    })
  },

  inputRemarks:function(e){
    // console.log('备注信息：',e.detail.value)
    this.setData({
      remarks: e.detail.value.replace(/\s+/g, '')
    })
  },

  clickAddBtn:function(e){
    var that = this;
    if (that.data.todayDate==''){
      wx.showToast({
        icon:'none',
        title: '请选择加班日期',
      })
      return;
    }
    if(that.data.money==''){
      wx.showToast({
        icon: 'none',
        title: '请输入加班费用',
      })
      return;
    }
    // console.log(that.data.todayDate,that.data.remarks)
    wx.showLoading({
      title: '',
    })
    const db = wx.cloud.database()
    db.collection('records').where({
      time: that.data.todayDate
    }).get({
      success(res) {
        if (res.data.length == 0) {
          console.log('记录查询成功可添加')
          db.collection('records').add({
            data: {
              userid: wx.getStorageSync('openid'),
              nickname: wx.getStorageSync('nickName'),
              time: that.data.todayDate,
              money: that.data.money,
              remarks: that.data.remarks
            },
            success(res) {
              console.log('记录添加成功')
              wx.hideLoading()
              wx.showToast({
                title: '添加成功',
                success(res) {
                  setTimeout(function () {
                    wx.navigateBack({

                    })
                  }, 1000)
                }
              })
            }
          })
        } else {
          wx.showToast({
            icon: 'none',
            title: '此日期已添加',
          })
        }
      }
    })
  },

})