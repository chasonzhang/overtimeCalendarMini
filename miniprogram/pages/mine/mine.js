// miniprogram/pages/mine/mine.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    hiddenQuery:true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    let openid = wx.getStorageSync('openid')
    if (openid =='o0gL15c7MTIdxlLP5Q2m-TlEfXNw'){
      this.setData({
        hiddenQuery: false
      })
    }
  },
  /**
   * 历史记录
   */
  clickHistory:function(e) {
    wx.navigateTo({
      url: '../historyRecord/historyRecord',
    })
  },
  /**
   * 加班查询
   */
  clickQuery:function(e){
    wx.navigateTo({
      url: '../query/query',
    })
  }
})