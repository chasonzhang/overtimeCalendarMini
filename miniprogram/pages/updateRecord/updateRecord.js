// miniprogram/pages/addRecord/addRecord.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    money: '',
    remarks: '',
    record:null,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let record = JSON.parse(options.record)
    this.setData({
      money: record.money,
      remarks: record.remarks,
      record: record
    })
  },

  inputMoney: function (e) {
    this.setData({
      money: e.detail.value
    })
  },

  inputRemarks: function (e) {
    // console.log('备注信息：',e.detail.value)
    this.setData({
      remarks: e.detail.value
    })
  },

  clickUpdateBtn: function (e) {
    var that = this;
    if (that.data.money == '') {
      wx.showToast({
        icon: 'none',
        title: '请输入加班费用',
      })
      return;
    }
    wx.showLoading({
      title: '',
    })
    wx.cloud.callFunction({
      name: 'updateRecord',
      data: {
        recordid: that.data.record._id,
        money: that.data.money,
        remarks: that.data.remarks
      },
      success: res => {
        console.log('[云函数] [updateRecord] 调用成功: ', res.result.stats.updated)
        wx.hideLoading()
        wx.showToast({
          title: '修改成功',
          success(res) {
            setTimeout(function () {
              wx.navigateBack({

              })
            }, 1000)
          }
        })
      },
      fail: err => {
        console.error('[云函数] [updateRecord] 调用失败', err)
      }
    })
  },

})