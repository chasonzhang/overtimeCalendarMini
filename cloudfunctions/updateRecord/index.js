// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({
  env: 'zhangchao-c91d58'
})

const db = cloud.database()

// 云函数入口函数
exports.main = async(event, context) => {
  try {
    return await db.collection('records').doc(event.recordid).update({
      data: {
        money: event.money,
        remarks: event.remarks
      },
    })
  } catch (e) {
    console.error(e)
  }
}